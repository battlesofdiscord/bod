import os

ROOT = os.path.join(os.getcwd(), "img")
DEFAULT_DUNGEON_PNG = os.path.join(ROOT, "default_dungeon.png")
