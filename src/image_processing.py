from PIL import Image, ImageFont, ImageDraw, ImageColor
import aiofiles
import io

async def open_image_async(file_path: str) -> Image :
    async with aiofiles.open(file_path, "rb") as file:
        contents = await file.read()
        try:
            img = Image.open(io.BytesIO(contents))
        except:
            img = None
    return img

if __name__ == "__main__":
    import asyncio
    import image_dir
    import time
    t = time.time_ns()
    img = asyncio.run(open_image_async(image_dir.DEFAULT_DUNGEON_PNG))
    t2 = time.time_ns()
    print(f"time: {(t2 - t) / 1000.0 / 1000.0}ms")

