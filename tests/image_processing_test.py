import pytest
from src.image_dir import DEFAULT_DUNGEON_PNG, ROOT
from src.font_dir import FREE_MONO
from src.image_processing import open_image_async

@pytest.mark.asyncio
async def test_open_image_async():
    img = await open_image_async(DEFAULT_DUNGEON_PNG) # A valid image SHOULD NOT return none
    assert img is not None
    img = await open_image_async(FREE_MONO) # An invalid image (font file) SHOULD return none
    assert img is None
    img = await open_image_async(ROOT) # A directory SHOULD return none
    assert img is None